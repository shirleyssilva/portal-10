import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosComponent } from './usuarios.component';
import { UsuariosService } from './usuarios.service';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    UsuariosComponent
  ],
  exports: [
    UsuariosComponent
  ],
  providers: [
    UsuariosService
  ]
})
export class UsuariosModule { }
