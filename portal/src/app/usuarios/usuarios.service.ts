import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor() { }

  getUsuarios(){
    return ['Shirley Silva', 'Pedro Rosa', 'Lucas Nogueira'];
  }
}
